package com.example.testapplication

import android.app.Application
import com.tenor.android.core.network.ApiClient
import com.tenor.android.core.network.ApiService
import com.tenor.android.core.network.IApiClient

class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        // Initialize the Tenor Core API
        val builder = ApiService.Builder(this, IApiClient::class.java)
        builder.apiKey(getString(R.string.tenor_api_key))

        ApiClient.init(this, builder)
    }

}