package com.example.testapplication.mainactivity

import com.tenor.android.core.model.impl.Tag
import com.tenor.android.core.view.IBaseView

interface IMainView : IBaseView {
    fun onReceiveReactionsSucceeded(tags: List<Tag>)

    fun onReceiveReactionsFailed(error: Throwable?)
}