package com.example.testapplication.mainactivity

import android.content.Context
import com.tenor.android.core.presenter.IBasePresenter
import com.tenor.android.core.response.impl.TagsResponse
import retrofit2.Call

interface IMainPresenter : IBasePresenter<IMainView> {
    fun getTags(context: Context, categories: List<String>?): Call<TagsResponse>?
}