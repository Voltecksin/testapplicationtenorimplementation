package com.example.testapplication.mainactivity

import android.content.Context
import android.text.TextUtils
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.tenor.android.core.constant.StringConstant
import com.tenor.android.core.model.impl.Tag
import com.tenor.android.core.network.ApiClient
import com.tenor.android.core.presenter.impl.BasePresenter
import com.tenor.android.core.response.impl.TagsResponse
import com.tenor.android.core.util.AbstractListUtils
import retrofit2.Call
import java.text.SimpleDateFormat
import java.util.*

class MainPresenter(view: IMainView) : BasePresenter<IMainView>(view), IMainPresenter {

    /**
     * Api call to fetch a list of Tag items
     * @param context
     * @param categories - optional field to pull specific types of tags.  Null if pulling from top of the results
     */
    override fun getTags(context: Context, categories: List<String>?): Call<TagsResponse>? {
        val categoriesSeparatedByCommas =
            //I know that abstractListUtils.isEmpty() checks for nullability
            if (categories != null && !AbstractListUtils.isEmpty(categories)) {
                TextUtils.join(StringConstant.COMMA, categories)
            } else {
                StringConstant.EMPTY
            }

        var call: Call<TagsResponse>? = null

        view?.let {
            call = ApiClient.getInstance(it.context)
                .getTags(
                    ApiClient.getServiceIds(it.context),
                    categoriesSeparatedByCommas,
                    getUtcOffset(context)
                )

            call?.enqueue(object : BaseWeakRefCallback<TagsResponse>(weakRef) {
                override fun success(@NonNull view: IMainView, @Nullable response: TagsResponse?) {
                    if (response == null || AbstractListUtils.isEmpty<Tag>(response.tags)) {
                        view.onReceiveReactionsFailed(Throwable())
                        return
                    }
                    view.onReceiveReactionsSucceeded(response.tags)
                }

                override fun failure(view: IMainView, throwable: Throwable?) {
                    view.onReceiveReactionsFailed(throwable)
                }
            })
        }
        return call
    }

    private fun getUtcOffset(context: Context?): String {
        return if (context == null) {
            ""
        } else {
            SimpleDateFormat("ZZZ", Locale.getDefault()).format(System.currentTimeMillis())
        }
    }
}