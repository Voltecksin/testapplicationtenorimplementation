package com.example.testapplication.mainactivity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.TranslateAnimation
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.testapplication.R
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.toolbar_search.*


class MainActivity : AppCompatActivity() {

    private var compositeDisposable = CompositeDisposable()
    private var searchTerm: String = ""

    private var searchItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(searchToolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_item_menu, menu)
        searchItem = menu?.findItem(R.id.m_search)

        searchItem?.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(p0: MenuItem?): Boolean {
                animateSearchToolbar(1, true, true)
                return true
            }

            override fun onMenuItemActionCollapse(p0: MenuItem?): Boolean {

                    if (searchItem != null && searchItem!!.isActionViewExpanded) {
                        animateSearchToolbar(1, false, false)
                    }

                return true
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onDestroy() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
        super.onDestroy()
    }

    fun animateSearchToolbar(numberOfMenuIcon: Int, containsOverflow: Boolean, show: Boolean) {
        searchToolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        window.statusBarColor = ContextCompat.getColor(this, R.color.quantum_grey_600)

        if (show) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val width = searchToolbar.width -
                        (if (containsOverflow) resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material) else 0) -
                        resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) * numberOfMenuIcon / 2
                val createCircularReveal = ViewAnimationUtils.createCircularReveal(
                    searchToolbar,
                    if (isRtl(resources)) searchToolbar.width - width else width,
                    searchToolbar.height / 2,
                    0.0f,
                    width.toFloat()
                )
                createCircularReveal.duration = 250
                createCircularReveal.start()
            } else {
                val translateAnimation =
                    TranslateAnimation(0.0f, 0.0f, -searchToolbar.height as Float, 0.0f)
                translateAnimation.duration = 220
                searchToolbar.clearAnimation()
                searchToolbar.startAnimation(translateAnimation)
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val width = searchToolbar.width -
                        (if (containsOverflow) resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material) else 0) -
                        resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) * numberOfMenuIcon / 2
                val createCircularReveal = ViewAnimationUtils.createCircularReveal(
                    searchToolbar,
                    if (isRtl(resources)) searchToolbar.width - width else width,
                    searchToolbar.height / 2,
                    width.toFloat(),
                    0.0f
                )
                createCircularReveal.duration = 250
                createCircularReveal.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        searchToolbar.setBackgroundColor(
                            getThemeColor(
                                this@MainActivity,
                                R.attr.colorPrimary
                            )
                        )
                        window.statusBarColor =
                            ContextCompat.getColor(this@MainActivity, R.color.quantum_grey_600)
                    }
                })
                createCircularReveal.start()
            } else {
                val alphaAnimation = AlphaAnimation(1.0f, 0.0f)
                val translateAnimation =
                    TranslateAnimation(0.0f, 0.0f, 0.0f, -searchToolbar.height as Float)
                val animationSet = AnimationSet(true)
                animationSet.addAnimation(alphaAnimation)
                animationSet.addAnimation(translateAnimation)
                animationSet.duration = 220
                animationSet.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {

                    }

                    override fun onAnimationEnd(animation: Animation) {
                        searchToolbar.setBackgroundColor(
                            getThemeColor(
                                this@MainActivity,
                                R.attr.colorPrimary
                            )
                        )
                    }

                    override fun onAnimationRepeat(animation: Animation) {

                    }
                })
                searchToolbar.startAnimation(animationSet)
            }
            window.statusBarColor =
                ContextCompat.getColor(this@MainActivity, R.color.quantum_grey_600)
        }
    }

    private fun isRtl(resources: Resources): Boolean {
        return resources.configuration.layoutDirection === View.LAYOUT_DIRECTION_RTL
    }

    private fun getThemeColor(context: Context, id: Int): Int {
        val theme = context.theme
        val a = theme.obtainStyledAttributes(intArrayOf(id))
        val result = a.getColor(0, 0)
        a.recycle()
        return result
    }
}
